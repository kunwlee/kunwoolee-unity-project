﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class checkAtBottom : MonoBehaviour {
	private GameController gameController;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent <GameController> ();
		}
		if (gameController == null) {
			Debug.Log("Cannot find 'GameController' script");
		}
	}

	// Update is called once per frame
	void Update () {
		if (transform.position.z < -5)
			gameController.GameOver ();
			
		
	}
}
