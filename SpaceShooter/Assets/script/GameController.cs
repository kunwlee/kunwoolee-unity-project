﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour 
{
	public GameObject hazard;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText pauseText;
	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;
	public GUIText instructionsText;
	private bool gameOver;
	private bool restart;
	private int score;

	private bool pause;
	private Rect windowRect;
	private bool paused = false, waited = true;
	private void waiting()
	{
		if (waited)
		if (Input.GetKeyDown (KeyCode.P)) {
			if (paused)
				paused = false;
			else
				paused = true;
			
			Invoke ("waiting", 0.3f);
		}
	}

	void setPause(bool ispaused){
		paused = ispaused;
		if (ispaused) {
			Time.timeScale = 0;
			pauseText.text = "PAUSED";
			pause = true;
		} else {
			Time.timeScale = 1.0f;
			pauseText.text = "    ";
			pause = false;
		}
	}


	void Start()
	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		UpdateScore ();
		StartCoroutine(SpawnWaves ());
		instructionsText.text = " Place your mouse corsor on top of the ship. Your mission: Protect the Earth from the asteroids.\n" +
			"Press any key when you are ready!!!             P key to pause"  ;
		Time.timeScale = 0;

	}

	void Update()
	{
		if (Input.anyKeyDown) {
			Time.timeScale = 1;
			instructionsText.text = "  ";
			Cursor.visible = false;

		}
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				Application.LoadLevel (Application.loadedLevel);
			}
		}


		if (Input.GetKeyDown (KeyCode.P)) 
			setPause (!paused);
	}

	IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds (startWait);
		while (true) 
		{
			for (int i = 0; i < hazardCount; i++) {
			
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver) {
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}
	public void AddScore(int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}
	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
		restart = true;
	}

	public void GameOver()
	{
		gameOverText.text = "You are screwed!";
		gameOver = true;

	}
}
